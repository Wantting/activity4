package Inheritance;
/**
 * Book
 */
public class Book {
    protected String title;
    protected String author;
    
    public Book(String title, String author){
        this.title=title;
        this.author=author;
    }

    public String getTitle() {
        return this.title;
    }
    
    public String getAuthor() {
        return this.author;
    }
     public String toString(){
         return this.title+": "+this.author;
     }
}