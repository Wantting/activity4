package Inheritance;

public class BookStore {
    
    public static void main(String[] args) {
        Book[] books=new Book[5];
       books[0]=new Book("Harry Potter", "JK");
       books[1]=new ElectronicBook("Le petit prince", "Antoine", 300);
       books[2]=new Book("GENIUS OR PSYCHOTIC","Gao Ming");
       books[3]=new ElectronicBook("L'Etranger", "Albert", 340);
       books[4]=new ElectronicBook("Hong Lou Meng", "Cao Xue Qing", 456);

  // System.out.println(b.books[1].getNumberBytes());
    //  b.books[0].getNumberBytes();
    //  b.books[1].getNumberBytes();
    //books[1].getNumberOfBytes();
     ElectronicBook b =(ElectronicBook) books[1];
     System.out.println(b.getNumberBytes());
     ElectronicBook e =(ElectronicBook) books[0];
     System.out.println(e.getNumberBytes());
    }
}
